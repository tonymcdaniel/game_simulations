Simple game simulations for demonstrating probability.

## coinflip.py

Simulation of repeatedly flipping a fair coin. Displays a plot of the distribution of the results.


## lottery.py

Simulation of playing the Powerball lottery. Compares the results from using a randomly generated ticket to using a fixed set of numbers for each play.


## monty.py

Simulation of Monty Hall's "Let's Make a Deal" gameshow. Compares winnings when swapping doors vs. staying with the initial pick.
