#!/usr/bin/env python
# encoding: utf-8
"""
File Name : monty.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Implementation of the Monty Hall game

Creation Date : Thu Feb 24, 2011  11:21AM
Last Modified : Thu Feb 24, 2011  11:46AM

Copyright (c) 2011 Tony McDaniel.

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import random

num_runs = 100
iterations = 0
swap_total = 0
noswap_total = 0
print "Games played         Swap Wins         No-Swap Wins"
while iterations < num_runs:
    random.seed()
    swap_wins = 0
    no_swap_wins = 0
    plays = 0
    num_plays = 1000

    while plays < num_plays:
        win_no = random.randint(1,3)
        my_no = random.randint(1,3)
        if win_no == my_no:
            no_swap_wins += 1
        else:
            swap_wins += 1
        plays += 1

    print "%d              %d                 %d " % (plays, swap_wins, no_swap_wins)
    swap_total += swap_wins
    noswap_total += no_swap_wins
    iterations += 1

print "Averages: "
print "%.1f percent swap wins" % (swap_total/(num_runs * 1000)*100)
print "%.1f percent no swap wins" % (noswap_total/(num_runs * 1000)*100)


