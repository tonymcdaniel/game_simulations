#!/usr/bin/env python
# encoding: utf-8
"""
File Name : coinflip.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Simulation of repeated flipping of a fair coin.

Creation Date : Wed Mar 02, 2011  11:21AM
Last Modified : Sun Dec 11, 2016  05:48PM

Copyright (c) 2011 Tony McDaniel.

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import random
import math
import numpy as np
import pylab as P

num_runs = 1000
iterations = 0
totals = []

while iterations < num_runs:
    headcount = 0
    random.seed()
    for i in range(50):
        b = random.randint(1,50)
        if b%2 == 0:
            headcount += 1
    totals.append(headcount)
    iterations += 1

x = np.array(totals)
mu = np.mean(x)
sigma = np.std(x)
# the histogram of the data with histtype='step'
bins = np.arange(0,51)
P.clf()
P.hist(x, bins,normed=1)

xvals = np.arange(0.0,50.1,0.1)
y = P.normpdf(xvals,mu,sigma)
P.plot(xvals,y,'k--',linewidth=1.5)
P.axvline(x=mu,color='r',ls='--',linewidth=1)
P.axvline(x=mu+sigma,color='g',ls='--',linewidth=1)
P.axvline(x=mu-sigma,color='g',ls='--',linewidth=1)

# add a line showing the expected distribution
P.show()
#P.savefig('plot.png',dpi=300)
