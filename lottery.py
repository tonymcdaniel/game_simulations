#!/usr/bin/env python
# encoding: utf-8
"""
File Name : lottery.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Implementation of the Power Ball lottery
Updated November 2012 for new Power Ball rules

Creation Date : Wed Mar 02, 2011  11:21AM
Last Modified : Wed Nov 28, 2012  11:50AM

Copyright (c) 2011 Tony McDaniel.

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import random

num_runs = 29200 # play once a day for 80 years
iterations = rand_total = same_total = rand_wins = same_wins = 0
prizes = [ 4, 4, 7, 7, 100, 100, 10000, 1000000, 500000000 ] 
probs = [ 55.41, 110.81, 706.43, 360.14, 12244.83, 19087.53, 648975.96, 5153632.65, 175223510.00 ]
rwin = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
swin = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
s_ticket = [ 1, 19, 6, 12, 30 ] # This is the ticket that we play every day
s_power = 13

while iterations < num_runs:
    rand_total -= 2 # buy the tickets :)
    same_total -= 2
    random.seed()
    r_ticket = []  # pick our random numbers
    for i in range(6):
        b = random.randint(1,59)
        while r_ticket.count(b) != 0:
            b = random.randint(1,59)
        r_ticket.append(b)
    r_power = random.randint(1,35)
    w_ticket = []  # draw the winning numbers
    for i in range(6):
        b = random.randint(1,59)
        while w_ticket.count(b) != 0:
            b = random.randint(1,59)
        w_ticket.append(b)
    w_power = random.randint(1,35)
# Check the random ticket
    matches = 0
    for num in w_ticket:
        matches += r_ticket.count(num)
    if r_power == w_power:
        if matches == 0:
            rand_total += prizes[0]
            rwin[0] += 1
        if matches == 1:
            rand_total += prizes[1]
            rwin[1] += 1
        if matches == 2:
            rand_total += prizes[2]
            rwin[2] += 1
        if matches == 3:
            rand_total += prizes[4]
            rwin[4] += 1
        if matches == 4:
            rand_total += prizes[6]
            rwin[6] += 1
        if matches == 5:
            rand_total += prizes[8]
            rwin[8] += 1
    else:
        if matches == 3:
            rand_total += prizes[3]
            rwin[3] += 1
        if matches == 4:
            rand_total += prizes[5]
            rwin[5] += 1
        if matches == 5:
            rand_total += prizes[7]
            rwin[7] += 1

# Check the same ticket
    matches = 0
    for num in w_ticket:
        matches += s_ticket.count(num)
    if s_power == w_power:
        if matches == 0:
            same_total += prizes[0]
            swin[0] += 1
        if matches == 1:
            same_total += prizes[1]
            swin[1] += 1
        if matches == 2:
            same_total += prizes[2]
            swin[2] += 1
        if matches == 3:
            same_total += prizes[4]
            swin[4] += 1
        if matches == 4:
            same_total += prizes[6]
            swin[6] += 1
        if matches == 5:
            same_total += prizes[8]
            swin[8] += 1
    else:
        if matches == 3:
            same_total += prizes[3]
            swin[3] += 1
        if matches == 4:
            same_total += prizes[5]
            swin[5] += 1
        if matches == 5:
            same_total += prizes[7]
            swin[7] += 1
    iterations += 1
samewins = 0
randomwins = 0
expect_single = 0.0
for i in range(len(prizes)):
    expect_single += 1.0*prizes[i]/probs[i]
    samewins += swin[i]
    randomwins += rwin[i]
expect_single -= 2.0*0.9686028257456829
total_wins = samewins + randomwins

print "Plays: %8d   Wins : %5d  %8.2f %%" % (2*iterations, total_wins, total_wins/iterations*50)
print "                  Same : %5d  %8.2f %%  " % (samewins,samewins/iterations*100)
print "                Random : %5d  %8.2f %% " % (randomwins,randomwins/iterations*100)
print " Prize         Same                Random           Odds"
print "-----------------------------------------------------------------"
for i in range(len(prizes)):
    print " $%9d  %5d  %8.7f%%   %5d  %8.7f%%  %8.7f%%" % (prizes[i],swin[i],swin[i]/iterations,rwin[i],rwin[i]/iterations,1.0/probs[i])
print "-----------------------------------------------------------------"
print "            Same         Random         Expected"
print "Total    :  $%11.2f      $%11.2f       $%11.2f " % (same_total, rand_total,expect_single*iterations)
print "Per Play :  $%11.2f      $%11.2f       $%11.2f " % (same_total/iterations, rand_total/iterations,expect_single)



